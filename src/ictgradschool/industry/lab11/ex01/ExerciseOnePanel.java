package ictgradschool.industry.lab11.ex01;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple GUI app that does BMI calculations.
 */
public class ExerciseOnePanel extends JPanel implements ActionListener {

    // TODO Declare JTextFields and JButtons as instance variables here.
    private JButton calculateBMIButton;
    private JButton calculateHealthyWeight;
    private JTextField height;
    private JTextField weight;
    private JTextField BMI;
    private JTextField healthyWeight;


    /**
     * Creates a new ExerciseOnePanel.
     */
    public ExerciseOnePanel() {
        setBackground(Color.white);

        // TODO Construct JTextFields and JButtons.
        this.calculateBMIButton = new JButton("Calculate BMI");
        this.calculateHealthyWeight = new JButton("Calculate Healthy Weight");
        this.height = new JTextField(15);
        this.weight = new JTextField(15);
        this.BMI = new JTextField(15);
        this.healthyWeight = new JTextField(15);


        // HINT: Declare them as instance variables so that other methods in this class (e.g. actionPerformed) can
        // also access them.

        // TODO Declare and construct JLabels
        // Note: These ones don't need to be accessed anywhere else so it makes sense just to declare them here as
        // local variables, rather than instance variables.
        JLabel BMILabel = new JLabel("Your Body Mass Index(BMI) is: ");
        JLabel healthyWeightLabel = new JLabel("Maximum Healthy Weight for your Height: ");
        JLabel heightLabel = new JLabel("Height in metres: ");
        JLabel weightLabel = new JLabel("Weight in kilograms: ");
        // TODO Add JLabels, JTextFields and JButtons to window.
        // Note: The default layout manager, FlowLayout, will be fine (but feel free to experiment with others if you want!!)

        this.setLayout(new FlowLayout());

        add(heightLabel);
        this.add(height);

        add(weightLabel);
        this.add(weight);

        add(calculateBMIButton);
        add(BMILabel);
        add(BMI);


        add(calculateHealthyWeight);
        add(healthyWeightLabel);
        add(healthyWeight);

        // TODO Add Action Listeners for the JButtons


        this.calculateBMIButton.addActionListener(this);
        this.calculateHealthyWeight.addActionListener(this);
    }


    /**
     * When a button is clicked, this method should detect which button was clicked, and display either the BMI or the
     * maximum healthy weight, depending on which JButton was pressed.
     */
    public void actionPerformed(ActionEvent event) {

        // TODO Implement this method.
        // Hint #1: event.getSource() will return the button which was pressed.
        // Hint #2: JTextField's getText() method will get the value in the text box, as a String.
        // Hint #3: JTextField's setText() method will allow you to pass it a String, which will be diaplayed in the text box.

        Object eventSource = event.getSource();

        if (eventSource == this.calculateBMIButton){
            double height = Double.parseDouble(this.height.getText());
            double weight = Double.parseDouble(this.weight.getText());
            double BMI = roundTo2DecimalPlaces(weight/(height*height));
            this.BMI.setText(Double.toString(BMI));

        }

        if (eventSource == this.calculateHealthyWeight){
            double height = Double.parseDouble(this.height.getText());
            double healthyWeight = roundTo2DecimalPlaces(24.9*height*height);
            this.healthyWeight.setText(Double.toString(healthyWeight));
        }




    }




    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     *
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}