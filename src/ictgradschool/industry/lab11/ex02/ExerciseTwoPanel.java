package ictgradschool.industry.lab11.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel implements ActionListener {


    private JTextField number1;
    private JTextField number2;
    private JTextField resultField;
    private JButton addButton;
    private JButton subtract;

    /**
     * Creates a new ExerciseFivePanel.
     */
    public ExerciseTwoPanel() {
        setBackground(Color.white);

        this.number1 = new JTextField(10);
        this.number2 = new JTextField(10);
        this.resultField = new JTextField(20);
        this.addButton = new JButton("Add");
        this.subtract = new JButton("Subtract");


        JLabel resultLabel = new JLabel("Result: ");

        this.setLayout(new FlowLayout());

        this.add(number1);
        this.add(number2);
        this.add(addButton);
        this.add(subtract);
        this.add(resultLabel);
        this.add(resultField);

        addButton.addActionListener(this);
        subtract.addActionListener(this);


    }

    public void actionPerformed(ActionEvent event) {

        Object eventSource = event.getSource();

        if (eventSource == addButton){

            double num1 = Double.parseDouble(this.number1.getText());
            double num2 = Double.parseDouble(this.number2.getText());
            double resultNum = num1+num2;
            this.resultField.setText(Double.toString(resultNum));

        }else if(eventSource == subtract){
            double num1 = Double.parseDouble(this.number1.getText());
            double num2 = Double.parseDouble(this.number2.getText());
            double resultNum = num1-num2;
            this.resultField.setText(Double.toString(resultNum));

        }




    }


    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}