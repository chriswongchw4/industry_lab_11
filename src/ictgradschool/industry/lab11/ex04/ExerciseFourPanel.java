package ictgradschool.industry.lab11.ex04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Displays an animated balloon.
 */
public class ExerciseFourPanel extends JPanel implements ActionListener, KeyListener {

    private List<Balloon> balloons = new ArrayList<>();
    private Timer timer;
    boolean pause = false;

    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseFourPanel() {
        setBackground(Color.white);

        balloons.add(new Balloon(30, 60));

        addKeyListener(this);
        timer = new Timer(300, this);

    }

    /**
     * Moves the balloon and calls repaint() to tell Swing we need to redraw ourselves.
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        for (Balloon balloon : balloons) {
            balloon.move();
            repaint();
        }

    }

    /**
     * Draws any balloons we have inside this panel.
     *
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {

        super.paintComponent(g);
        for (Balloon balloon : balloons) {
            balloon.draw(g);
            repaint();
        }

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {


        int direction = e.getKeyCode();


        for (Balloon balloon : balloons) {
            if (direction == KeyEvent.VK_UP) {
                balloon.setDirection(Direction.Up);
            } else if (direction == KeyEvent.VK_DOWN) {
                balloon.setDirection(Direction.Down);
            } else if (direction == KeyEvent.VK_RIGHT) {
                balloon.setDirection(Direction.Right);
            } else if (direction == KeyEvent.VK_LEFT) {
                balloon.setDirection(Direction.Left);
            }

            balloon.move();
            requestFocusInWindow();
            repaint();
            timer.start();
            pause = false;

            if (direction == KeyEvent.VK_S) {
                timer.stop();
                pause = true;
            }
        }

        if (direction == KeyEvent.VK_A) {

            balloons.add(new Balloon(((int) (Math.random() * 100 + 30)), ((int) (Math.random() * 100 + 60))));
            repaint();
        }


    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}